"""
Simple dictionnire de correspondance entre
l'univers et le dictionnaire.
Cela pour passer l'univers en parametre
dans notre fonction
""" 

from Dico import dico_lord_of_the_ring
from Dico import dico_star_wars
from Dico import dico_super_hero

dico_des_dico_prenom = {
    'star_wars': dico_star_wars.dico_correspondance_star_wars_prenom,
    'lord_of_the_ring':dico_lord_of_the_ring.dico_correspondance_lord_of_the_ring_prenom,
    'super_hero':dico_super_hero.dico_correspondance_super_hero_prenom,
}

dico_des_dico_nom = {
    'star_wars': dico_star_wars.dico_correspondance_star_wars_nom,
    'lord_of_the_ring':dico_lord_of_the_ring.dico_correspondance_lord_of_the_ring_nom,
    'super_hero':dico_super_hero.dico_correspondance_super_hero_nom,
}