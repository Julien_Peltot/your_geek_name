"""
Simple dictionnire de correspondance entre
la 1er lettre du prénom puis celle du nom.
Cela pour crée votre nom dans l'univers de
Lord Of The Ring !
""" 

dico_correspondance_lord_of_the_ring_prenom = {
    'A':'Tuk',
    'B':'Gardner',
    'C':'Bunce',
    'D':'Dudo',
    'E':'Hunald',
    'F':'Sago',
    'G':'Drogon',
    'H':'Chlodmer',
    'I':'Hubert',
    'J':'Leufroy',
    'K':'Vulmar',
    'L':'Tomba',
    'M':'Wilibald',
    'N':'Mualón',
    'O':'Tellésó',
    'P':'Citot',
    'Q':'Sires',
    'R':'Seöméso',
    'S':'Thalmandó',
    'T':'Pharém',
    'U':'Oruitha',
    'V':'Nuasin',
    'W':'Shotham',
    'X':'Wéphóra',
    'Y':'Iephiró',
    'Z':'Saesun',

 }
dico_correspondance_lord_of_the_ring_nom = {
    'B':'Twofoot',
    'A':'Clayhanger',
    'C':'Roper',
    'D':'Brown',
    'E':'Uldat',
    'F':'Ullati',
    'G':'Samo',
    'H':'Pegardiot',
    'I':'Keg',
    'J':'Toid',
    'K':'Mhaima',
    'L':'Ganrar',
    'M':'Thiruk',
    'N':'Gronak',
    'O':'Frelbom',
    'P':'Kerven',
    'Q':'Gomo',
    'R':'Segruc',
    'S':'Mizgud',
    'T':'Gargokh',
    'U':'Shaurbaur',
    'V':'Dozru',
    'W':'Ulfu',
    'X':'Aucderg',
    'Y':'Azze',
    'Z':'Uthrid',


}
















