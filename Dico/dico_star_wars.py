"""
Simple dictionnire de correspondance entre
la 1er lettre du prénom puis celle du nom.
Cela pour crée votre nom dans l'univers de 
Star Wars !
""" 

dico_correspondance_star_wars_prenom = { 
    'A':'Jonas',
    'B':'Joran',
    'C':'Leon',
    'D':'Ismaeln',
    'E':'Crev',
    'F':'Xakic',
    'G':'Drosk',
    'H':'Ubbal',
    'I':'Ranneth',
    'J':'Adan',
    'K':'Unie',
    'L':'Jalaila',
    'M':'Krava',
    'N':'Towan',
    'O':'Daynar',
    'P':'Daniel',
    'Q':'Rainee',
    'R':'Janeth',
    'S':'Oret',
    'T':'Rallyn',
    'U':'Zoserab',
    'V':'Kendall',
    'W':'Arkanus',
    'X':'Luha',
    'Y':'Gul',
    'Z':'Claudek',  
}

dico_correspondance_star_wars_nom  = {
    'A':'Verbeke',
    'B':'Cyn',
    'C':'Eldrel',
    'D':'Melkans',
    'E':'Muro',
    'F':'Convarion',
    'G':'Lee',
    'H':'Obdris',
    'I':'Minetii',
    'J':'Trenor',
    'K':'Anarth',
    'L':'Horain',
    'M':'Odan',
    'N':'Wan',
    'O':'Bolera',
    'P':'Tardun',
    'Q':'Atoka',
    'R':'Leandar',
    'S':'Kasra',
    'T':'Aphax',
    'U':'Pag',
    'V':'Steveus',
    'W':'Calla',
    'X':'Halos',
    'Y':'Harna',
    'Z':'Pegardo',
}
