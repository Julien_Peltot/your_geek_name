"""
Simple dictionnire de correspondance entre
la 1er lettre du prénom puis celle du nom.
Cela pour crée votre nom dans L'univers des
Super Héros !
""" 

dico_correspondance_super_hero_prenom = {
    'A':'Cat',
    'B':'Combat',
    'C':'Computer',
    'D':'Divine',
    'E':'Ethereal',
    'F':'Hunt',
    'G':'Iron',
    'H':'Laugh',
    'I':'Metal',
    'J':'Nemesis',
    'K':'Radioactive',
    'L':'Rapid',
    'M':'Raptor',
    'N':'Screaming',
    'O':'Sham',
    'P':'Spectre',
    'Q':'Spike',
    'R':'Spike',
    'S':'Street',
    'T':'Track',
    'U':'Under',
    'V':'Maxi',
    'W':'Supra',
    'X':'Big',
    'Y':'Red',
    'Z':'Ultra',  
}

dico_correspondance_super_hero_nom = {
    'A':'Rapier',
    'B':'Haunt',
    'C':'Hurricane',
    'D':'beauty',
    'E':'Sorceress',
    'F':'Raven',
    'G':'Ember',
    'H':'Cut',
    'I':'Leaf',
    'J':'Cleaver',
    'K':'Draft',
    'L':'Blast',
    'M':'Queen',
    'N':'Strike',
    'O':'Gleam',
    'P':'Axe',
    'Q':'Maw',
    'R':'Panther',
    'S':'Lass',
    'T':'Serpent',
    'U':'Lion',
    'V':'Delahaye',
    'W':'Pick',
    'X':'Thief',
    'Y':'Bear',
    'Z':'Python',
}