from tkinter import *
from tkinter.messagebox import *
import tkinter.font as tkFont
from creation_new_name import *

# création de la fenêtre tkinter
fenetre = Tk()
fenetre.config(bg='grey')
fenetre.title("Votre nom geek !")

# création des polices d'écritures
helv14 = tkFont.Font(family='Helvetica',size=14)
font_titre = tkFont.Font(family='Helvetica', size=25, weight='bold')

# association de la fonction pour créé son nouveau nom
# avec l'univers star wars
def button_star_wars_callback():
    retour.set(associer_au_dico(value.get(),'star_wars'))

# association de la fonction pour créé son nouveau nom
# avec l'univers lord of the ring   
def button_lord_of_the_ring_callback():
    retour.set(associer_au_dico(value.get(),'lord_of_the_ring')) 

# association de la fonction pour créé son nouveau nom
# avec l'univers super hero
def button_super_hero_callback():
     retour.set(associer_au_dico(value.get(),'super_hero'))

# création du titre
typetitre = StringVar()
typetitre.set("Quel est ton nom Geek ?")
titre = Label(fenetre,textvariable=typetitre)
titre.grid(column=1, row=0,padx = 10,pady=10)
titre.config( width = 30, font = font_titre, bg='grey')

# création du champ de saisie
value = StringVar()
value.set("Entrez votre prénom puis votre nom..")
input_prenom_nom = Entry(fenetre, textvariable=value)
input_prenom_nom.grid(column=1, row=1,padx = 10,pady=10 )
input_prenom_nom.config( width = 30, font = helv14, bg='dark grey' ) 

# création du champ resultat
retour = StringVar()
output_resultat = Entry(fenetre, textvariable = retour)
output_resultat.grid(column = 1, row=2,padx = 10,pady=10 ) 
output_resultat.config(width = 30, font = helv14,bg='dark grey' )

# création du bouton staw wars
button_star_wars = Button(fenetre, text="Star Wars", command=button_star_wars_callback)
button_star_wars.grid(row=3, column=0,padx = 30,pady=10 )
button_star_wars.config(  bg = 'dark orange', fg = 'black', font = helv14)

# création du bouton lord of the ring
button_lord_of_the_ring = Button(fenetre, text="Lord Of The Ring", command=button_lord_of_the_ring_callback)
button_lord_of_the_ring.grid(row=3, column=1, padx = 10,pady=10 )
button_lord_of_the_ring.config(  bg = 'dark orange', fg = 'black', font = helv14)

# création du bouton super hero
button_super_hero = Button(fenetre, text="Super Hero",command=button_super_hero_callback)
button_super_hero.grid(row =3 ,column = 2,padx = 30,pady=10  )
button_super_hero.config(  bg = 'dark orange', fg = 'black', font = helv14)

fenetre.mainloop()