import unittest
from creation_new_name import *
from Dico import *

class TestNewNameFunctions(unittest.TestCase):
   
    
    def test_split_name(self):
        
        """ Test de la séparation de la chaine de caractères saisie
        par l'utilisateur  à une lise de chaque lettre."""
        
        self.assertEqual(separer_chaine('jul pel'), ['J', 'U', 'L',  ' ', 'P', 'E', 'L' ])
        self.assertEqual(separer_chaine('ypel'), ['Y', 'P', 'E', 'L'])
        self.assertEqual(separer_chaine('samy lemeilleurprof'),['S','A','M','Y',' ','L','E','M','E','I','L','L','E','U','R','P','R','O','F'])
        self.assertEqual(separer_chaine('666'), ['6', '6', '6'])

   
    def test_deux_lettre(self):

        """ Test de la créations d'une chaine de caractères
        de la premiere lettre du nom et prenom 
        si l'utilisateur saisie n'importe quoi on prend 'AA'
        comme valeur par défaut."""

        self.assertEqual(prendre_les_deux_premiere_lettre(['J', 'U', 'L',  ' ', 'P', 'E', 'L']), 'JP')
        self.assertEqual(prendre_les_deux_premiere_lettre(['P', 'E','P']),'PE')
        self.assertEqual(prendre_les_deux_premiere_lettre(['6', '7', '6']),'AA')
        self.assertEqual(prendre_les_deux_premiere_lettre(['(', '%', '6']),'AA')

    def test_dico(self):

        """ Test de la bonne coordonance entre les deux lettres 
        et les dico des univers."""

        self.assertEqual(associer_au_dico('ZZ','star_wars'),'Claudek Pegardo')
        self.assertEqual(associer_au_dico('BM','super_hero'),'Combat Queen')
        self.assertEqual(associer_au_dico('ZM','lord_of_the_ring'),'Saesun Thiruk')


