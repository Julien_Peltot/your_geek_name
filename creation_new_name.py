from Dico import dico_general
from Dico import dico_vierge

def separer_chaine(chaine_nom):
    """ Sépare une chaine de caractères 
    en une liste des lettres de cette chaine
    en majuscule.""" 

    nom_prenom = chaine_nom.split()
    nom_prenom_maj = [lettre.upper() for lettre in chaine_nom] 

    return nom_prenom_maj

def prendre_les_deux_premiere_lettre(nom_prenom_maj):
    """Retourne une chaine de caractères avec 
    la première lettre du prénom puis la première
    lettre du nom. Si cela est impossible
    (saisie incorrecte) on prendra 'AA' comme
    valeur par défaut. Si il y à que un nom/prénom
    on prends les deux premières lettres."""

    # on obtient la lettre prénom
    if nom_prenom_maj[0]  not in  dico_vierge.dico_correspondance_prenom:
        nom_prenom_maj[0] = 'A'
    lettre_prenom = nom_prenom_maj[0]

    # le nom commence juste après le 'separator'
    if (' ') not in nom_prenom_maj:
        separator = 0
    else:
        separator = nom_prenom_maj.index(' ') 

    # on obtient la lettre nom
    if nom_prenom_maj[1]  not in  dico_vierge.dico_correspondance_nom:
        nom_prenom_maj[1] = 'A'
    lettre_nom = nom_prenom_maj[separator+1]

    deux_premiere_lettre = lettre_prenom + lettre_nom

    return deux_premiere_lettre

def associer_au_dico(nom_user,univers):

    """ On associe la lettre du prénom et celle du nom
    au dico avec l'univers passé en paramètre (fixé pour
    chaque bouton dans le tkinter) puis on retourne
    le nouveau prénom suivi du nouveau nom."""

    saisie_user = separer_chaine(nom_user)
    deux_premiere_lettre = prendre_les_deux_premiere_lettre(saisie_user)

    lettre_prenom = deux_premiere_lettre[0]
    lettre_nom = deux_premiere_lettre[1] 

    # on associe les lettres avec le bon univers
    dico_prenom = dico_general.dico_des_dico_prenom[univers]
    dico_nom =  dico_general.dico_des_dico_nom[univers]

    new_prenom = dico_prenom[lettre_prenom]
    new_nom = dico_nom[lettre_nom]

    return f'{new_prenom} {new_nom}'
    